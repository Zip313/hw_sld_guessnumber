﻿// See https://aka.ms/new-console-template for more information
using HW_SOLID_GuessNumber.Services.Implementations;
using HW_SOLID_GuessNumber.Services.Interfaces;

IGameService gameService = GameService.GetInstance(new GameGuessNumber());
gameService.StartGame();