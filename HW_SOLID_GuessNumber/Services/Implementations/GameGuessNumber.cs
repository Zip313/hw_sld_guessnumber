﻿using HW_SOLID_GuessNumber.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SOLID_GuessNumber.Services.Implementations
{
    /// <summary>
    /// Игра в угадай число, загадано число от 0 до 9,число попыток 3
    /// </summary>
    internal class GameGuessNumber: IGame
    {
        private int Number { get; set; }
        private int Tryes { get; set; } = 0;
        private bool isGameOver { get; set; }
        private bool isWin { get; set; }

        public GameGuessNumber()
        {
            Random random = new Random();
            this.Number = random.Next(0,9);
        }

        public string CheckAnswer(char x)
        {
            if (x == 'r')
            {
                Tryes = 0;
                isGameOver = false;
                this.Number = new Random().Next(0, 9);
                return "Игра началась сначала";
            }
            int answer = 0;
            if(!int.TryParse(x.ToString(), out answer)) return "Неверный формат ответа, требуется число" ;
            if(isGameOver) return GetResults();
            this.Tryes++;
            this.isGameOver = this.Tryes >= 3 || answer == this.Number;
            this.isWin = answer == this.Number;
            return GetResults();
        }

        private string GetResults()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(isGameOver ? "Игра окончена " : "Игра продолжается ");
            if (isGameOver) sb.Append(isWin ? $"Игрок победил, правильный ответ {this.Number} " : $" Игрок проиграл, правильный ответ {this.Number} ");
            sb.Append($" использовано попыток {Tryes}");
            return sb.ToString();

        }

        public string GetRules()
        {
            return "Игра в угадай число, загадано число от 0 до 9, попробуйте угадать его с 3 попыток.";
        }

        public string GetNextInstruction()
        {
            if (isGameOver) return "Игра окончена, для перезапуска нажмите r";
            return $"Введите число от 0 до 9, у вас осталось {3-Tryes} попыток...";
        }
    }
}
