﻿using HW_SOLID_GuessNumber.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SOLID_GuessNumber.Services.Implementations
{
    public class GameService : IGameService
    {
        private static GameService instance;
        private IGame game;
        public static GameService GetInstance(IGame game)
        {
            if (instance == null)
                GameService.instance = new GameService(game);
            else 
                GameService.instance.game = game;
            return instance;
        }



        private GameService(IGame game)
        {
            this.game = game;
        }

        public void StartGame()
        {
            Console.WriteLine("Игра началась, для выхода необходимо нажать клавишу q");
            Console.WriteLine(game.GetRules());
            
            char key = '0';
            while (key != 'q')
            {
                Console.WriteLine(game.GetNextInstruction());
                key = Console.ReadKey().KeyChar;
                Console.WriteLine();
                if (key == 'q') break;
                Console.WriteLine(game.CheckAnswer(key));
            }

            Console.WriteLine("Вы вышли из игры");
        }
    }
}
