﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SOLID_GuessNumber.Services.Interfaces
{
    public interface IGame : IGameRules
    {
        string CheckAnswer(char x);
        string GetNextInstruction();
    }
}
