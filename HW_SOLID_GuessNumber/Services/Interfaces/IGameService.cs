﻿using HW_SOLID_GuessNumber.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SOLID_GuessNumber.Services.Interfaces
{
    internal interface IGameService
    {
        void StartGame();
    }
}
